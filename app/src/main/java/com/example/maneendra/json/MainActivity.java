package com.example.maneendra.json;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;


public class MainActivity extends ActionBarActivity {

    //read json
    public String readJSONFeed(String URL){

        StringBuilder stringBuilder = new StringBuilder();
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(URL);
        try {
            HttpResponse response = httpClient.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            Log.e("JSON", "status code " + statusCode);
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                inputStream.close();
            }
            else {
                Log.d("JSON", "Failed to download file");
            }
        }
        catch (Exception e){
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        Log.e("JSON", "stringBuilder " + stringBuilder.toString());
        return stringBuilder.toString();
    }

    //async task
    private class ReadWeatherJSONFeedTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            Log.e("JSON", "url " + urls[0]);
            return readJSONFeed(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                /*JSONObject jsonObject = new JSONObject(result);
                JSONObject weatherObservationItems = new JSONObject(jsonObject.getString("weatherObservation"));
                Toast.makeText(getBaseContext(),
                        weatherObservationItems.getString("clouds") +
                                " - " + weatherObservationItems.getString("stationName"),
                        Toast.LENGTH_LONG).show();*/

                JSONArray json = new JSONArray(result);
                for(int i=0;i<json.length();i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject e = json.getJSONObject(i);
                    Toast.makeText(getBaseContext(),
                            e.getString("title") +
                                    " - " + e.getString("description"),
                            Toast.LENGTH_LONG).show();
                }



            }
            catch (Exception e){
                Log.e("ReadWeatherJSONFeedTask", e.getLocalizedMessage());
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new ReadWeatherJSONFeedTask().execute(
                "http://192.168.1.2:8080/RESTfullProject/REST/WebService/GetFeeds");

    }

    public void btnGetWeather(View view) {
        EditText txtLat = (EditText) findViewById(R.id.txtLat);
        EditText txtLong = (EditText) findViewById(R.id.txtLong);

        /*new ReadWeatherJSONFeedTask().execute(
                "http://ws.geonames.org/findNearByWeatherJSON?lat=" +
                        txtLat.getEditableText().toString() + "&lng=" +
                        txtLong.getText().toString() + "&username=maneendra");
                        */
        new ReadWeatherJSONFeedTask().execute(
                "http://localhost:8080/RESTfullProject/REST/WebService/GetFeeds");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
